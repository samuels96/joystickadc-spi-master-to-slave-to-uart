#include "uart.h"

void UART_init()
{
	UBRR0H = (BAUDRATE >> 8);
	UBRR0L = BAUDRATE;
	UCSR0B |= (1<<TXEN0) | (1<<RXEN0);
	UCSR0C |= (3<<UCSZ00);
}


void UART_send(unsigned char ch)
{
	send(ch);
	terminate();
}

void UART_sendString(char str[], short len)
{
	for(int i = 0; i < len; i++)
	{
		send(str[i]);
	}
	terminate();
}

unsigned char UART_receive(void)
{
	while (!(UCSR0A & (1<<RXC0)));
	return UDR0;

}

void UART_receiveString(char str[], short len)
{
	for(short i = 0; i < len; i++)
	{
		while (!(UCSR0A & (1<<RXC0)));
		str[i] = UDR0;
		if(str[i] == 13) { str[i] = '\0'; return; }
	}
	str[len-1] = '\0';
}

void send(unsigned char ch)
{
	while(!(UCSR0A & (1<<UDRE0)));
	UDR0 = ch;
}

void terminate()
{
	while(!(UCSR0A & (1<<UDRE0)));
	UDR0 = '\r';
	while(!(UCSR0A & (1<<UDRE0)));
	UDR0 = '\n';
}