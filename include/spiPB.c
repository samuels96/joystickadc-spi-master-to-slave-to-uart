#include "spiPB.h"

// ATMEGA328PB
void spi_initMasterPB()
{
    DDRB |= (1<<2)|(1<<3)|(1<<5);    // SCK, MOSI and SS as outputs
	DDRB &= ~(1<<4);                 // MISO as input

	SPCR0 |= (1<<MSTR);               // Set as Master
	SPCR0 |= (1<<SPI2X) | (1<<SPR0);
	SPCR0 |= (1<<SPE);                // Enable SPI
}

void spi_initSlavePB()
{
    DDRB &= ~((1<<2)|(1<<3)|(1<<5));   // SCK, MOSI and SS as inputs
	DDRB |= (1<<4);                    // MISO as output
    
    SPCR0 &= ~(1<<MSTR);                // Set as slave
	SPCR0 |= (1<<SPI2X) | (1<<SPR0);       // divide clock by 128
	SPCR0 |= (1<<SPE);                  // Enable SPI
}

void spi_sendPB(unsigned char ch) 
{
	SPDR0 = ch;		
	while(!(SPSR0 & (1<<SPIF)));
}


unsigned char spi_receivePB() 
{
	while(!(SPSR0 & (1<<SPIF)));    // wait until all data is received
    return SPDR0;
}

unsigned char spi_transmit(unsigned char ch)
{
	SPDR0 = ch;	
	while(!(SPSR0 & (1<<SPIF)));
	return SPDR0;
}