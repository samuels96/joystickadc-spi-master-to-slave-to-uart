#include "spi.h"

// ATMEGA328P (ARDUINO UNO)

void spi_initMaster()
{
    DDRB |= (1<<2)|(1<<3)|(1<<5);    // SCK, MOSI and SS as outputs
	DDRB &= ~(1<<4);                 // MISO as input

	SPCR |= (1<<MSTR);               // Set as Master
	SPCR |= (1<<SPR0) | (1<<SPI2X);     // divided clock by 8
	SPCR |= (1<<SPE);                // Enable SPI
	
}

void spi_initSlave()
{
    DDRB &= ~((1<<2)|(1<<3)|(1<<5));   // SCK, MOSI and SS as inputs
	DDRB |= (1<<4);                    // MISO as output
    
    SPCR &= ~(1<<MSTR);                // Set as slave
	SPCR |= (1<<SPR0) | (1<<SPI2X);       // divide clock by 8

	SPCR |= (1<<SPE);                  // Enable SPI
}


void spi_send(unsigned char ch) 
{
	SPDR = ch;
	while(!(SPSR & (1<<SPIF)));
}

unsigned char spi_receive()
{
    while(!(SPSR & (1<<SPIF)));    // wait until all data is received
    return SPDR;
}

unsigned char spi_transmit(unsigned char ch)
{
	SPDR = ch;
	while(!(SPSR & (1<<SPIF)));
	return SPDR;
}
