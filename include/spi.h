#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#include <util/delay.h>
#include <avr/io.h>

// ATMEGA328P (ARDUINO UNO)
void spi_initMaster();
void spi_initSlave();
void spi_send(char ch); 
char spi_receive();
unsigned char spi_transmit(unsigned char ch);
