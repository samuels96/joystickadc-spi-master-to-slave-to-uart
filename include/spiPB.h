#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#include <util/delay.h>
#include <avr/io.h>

// ATMEGA328PB
void spi_initMasterPB(); 
void spi_initSlavePB();
void spi_sendPB(char ch); 
char spi_receivePB();
unsigned char spi_transmit(unsigned char ch);
